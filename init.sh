#!/bin/bash
export PATH=/bin:/sbin:/usr/bin:/usr/sbin
export LANG=C
export SHELL=/bin/bash
export TERM=linux
export DEBIAN_FRONTEND=noninteractive
mount -o remount,rw /
if [[ -f /bin/systemd-tmpfiles ]] ; then
    /bin/systemd-tmpfiles --create # systemd way
elif [[ -f /bin/tmpfiles ]] ; then
    tmpfiles --create # openrc-tmpfiles way
fi
if [[ -f /lib/systemd/systemd-udevd ]] ; then
    /lib/systemd/systemd-udevd --daemon # systemd-udev way
elif [[ -f /sbin/udevd ]] ; then
    /sbin/udevd --daemon # eudev way
fi
udevadm trigger -c add &>> /netinstall-combo/log
udevadm settle &>> /netinstall-combo/log
if [[ ! -d /dev/shm ]] ; then
    mkdir -p /dev/shm
    mount -t tmpfs tmpfs /dev/shm
fi
mkdir -p /run/dbus
dbus-daemon --system --nofork &>> /netinstall-combo/log &
sleep 1
NetworkManager -d &>> /netinstall-combo/log &
cd /netinstall-combo
while true ; do
    setsid busybox cttyhack bash main.sh
    read -n 1
done
