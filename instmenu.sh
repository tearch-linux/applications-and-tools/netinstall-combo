source "${PROGRAM}"/function.sh
export rootfs=""
export efifs=""
export distro=""
export profile=""
instmenu(){
    opt=$(TITLE="Installation Menu\n$(write_info)" menu \
        1 "Mount-rootfs" 2 "Select-distro" 3 "Select-profile" \
        $([[ "$distro" != "" && $(type -t distro_configure) == function ]] && echo "4 Configure-distro" ) \
        $([[ "$distro" != "" && "$rootfs" != "" && ! -d /sys/firmare/efi || "$efifs" != "" ]] && echo "5 Install") \
        0 "Back")
    case $opt in
        1) # mount rootfs
            export rootfs=""
            export efifs=""
            mount_rootfs
            ;;
        2) # select distro
            export distro=""
            select_distro
            ;;
        3) # select profile
            export profile=""
            select_profile
            ;;
        4) # configure distro
            distro_configure
            ;;
        5) # install
            installation
            bind_mount
            for p in common ${profile[@]} ; do
                if [[ $(type -t install_$p ) == function ]] ; then
                    install_$p
                fi
            done
            create_user
            write_fstab
            grub_install
            ;;
        0)
            return 0
            ;;
    esac
    instmenu
}

write_info(){
    echo " - Root partition : $rootfs\n"
    if [[ -d /sys/firmware/efi ]] ; then
        echo " - Efi  partition : $efifs\n"
    fi
    echo " - Distribution   : $distro\n"
    echo " - Profile        : $profile\n"
    
}

list_distros(){
    ls "${PROGRAM}"/distro | while read line ; do
        echo " ${line/.sh/} $(cat ${PROGRAM}/distro/$line | grep "^comment=" | head -n 1 | sed "s/^comment=//g")"
    done
}

unset_distro(){
    for p in common ${profile[@]} ; do
        unset install_$p
    done
    unset distro_configure tool_init installation
    unset distro populate_user profile
}

select_distro(){
    [[ "$distro" == "" ]] || return 0
    opt=$(TITLE="Select Distribution" menu $(list_distros))
    [[ $? -ne 0 ]] && return 0
    unset_distro
    export distro=$opt
    source "${PROGRAM}"/distro/${opt,,}.sh
    set +e
    tool_init || msg_console "Failed to install toolkit $(unset_distro)"
    set -e
}

select_profile(){
    set +e
    [[ "$distro" == "" ]] && select_distro
    export profile=$(TITLE="Select Profile for $distro" list $(get_profiles))
    [[ "$profile" == "none" ]] && export profile=""
    set -e
}

mount_rootfs(){
    set +e
    mkdir -p /target
    while ! mount /dev/$rootfs /target && [[ ! -b /dev/$rootfs ]] ; do
        export rootfs=$(TITLE="Select root partition" select_part $(TITLE="Select MBR for /" select_disk))
        umount -lf /target/ || true
    done
    if [[ -d /sys/firmware/efi ]] ; then
        mkdir -p /target/boot/efi
        while ! mount /dev/$efifs /target/boot/efi && [[ ! -b /dev/$efifs ]] ; do
            export efifs=$(TITLE="Select efi partition" select_part $(TITLE="Select MBR for /boot/efi" select_disk))
            umount -lf /target/boot/efi || true
        done
    fi
    set -e
}

create_user(){
    while [[ "$user" == "" ]] ; do
        user=$(TITLE="What is new user name" input)
    done
    while [[ "$pass" == "" ]] ; do
        pass=$(TITLE="Provide a password for $user" input)
    done
    chroot /target useradd -m "$user" -s /bin/bash
    chroot /target usermod -p $(chroot /target openssl passwd "$pass") "$user"
    chroot /target usermod -p $(chroot /target openssl passwd "$pass") "root"
    populate_user
}

bind_mount(){
    mount --bind /dev /target/dev
    mount --bind /dev/pts /target/dev/pts
    mount --bind /sys /target/sys
    mount --bind /proc /target/proc
    mount --bind /run /target/run
}

grub_install(){
    if [[ -d /sys/firmware/efi ]] ; then
        chroot /target mount -t efivarfs efivarfs /sys/firmware/efi/efivars
        chroot /target mount /dev/$efifs /boot/efi
    fi
    while [[ ! -b "/dev/$mbr" ]] ; do
        mbr=$(TITLE="select MBR for grub" select_disk)
    done
    chroot /target grub-install /dev/$mbr --removable
    chroot /target grub-mkconfig -o /boot/grub/grub.cfg
}

write_fstab(){
    type=$(blkid | grep /dev/$rootfs | sed "s/.*TYPE=\"//g;s/\".*//g")
    echo "/dev/$rootfs / $type defaults,rw 0 0" >> /target/etc/fstab
}
