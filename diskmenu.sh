source "${PROGRAM}"/function.sh
diskmenu(){
    opt=$(TITLE="Disk Menu\n$(write_info)" menu \
        1 "Edit-partition" 2 "Format-disk" 0 "Back")
    case $opt in
        1) # partitioning
            cfdisk /dev/$(select_disk) || msg "Failed to run cfdisk"
            ;;
        2) # format disk
            part=$(select_part $(select_disk))
            filesystem=$(TITLE="Select filesystem" menu \
            "ext4" "Standard-linux-filesystem" \
            "vfat" "Fat32-filesystem" \
            "xfs"  "High-performance-filesystem" \
            "ntfs" "Winzort-filesystem" \
            "f2fs" "Flash-friedly-filesystem" \
            0      "Back")
            umount -lf /dev/$part || true
            yes | mkfs.$filesystem /dev/$part
            ;;
        0)
            return 0
            ;;
    esac
    diskmenu
}
