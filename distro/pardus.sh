#!/bin/bash
source ${PROGRAM}/distro/debian.sh
comment="For-freedom"
debian_repo="https://depo.pardus.org.tr/pardus"
debian_dist="yirmibir"


eval debian_"$(declare -f get_profiles)"
get_profiles(){
    debian_get_profiles
    echo -ne " libreoffice Latest-version-of-libreoffice false"
    echo -ne " theme pardus-themes true"
}

install_common(){
    echo "deb http://depo.pardus.org.tr/pardus ondokuz main contrib non-free" > /target/etc/apt/sources.list
    echo "deb http://depo.pardus.org.tr/guvenlik ondokuz main contrib non-free" >> /target/etc/apt/sources.list
    echo "deb http://depo.pardus.org.tr/pardus yirmibir main contrib non-free" > /target/etc/apt/sources.list
    echo "deb http://depo.pardus.org.tr/guvenlik yirmibir main contrib non-free" >> /target/etc/apt/sources.list
    chroot /target apt update
    chroot /target apt full-upgrade -y
    chroot /target apt install sudo lsb-release -y
}

eval debian_"$(declare -f install_gnome)"
install_gnome(){
    debian_install_gnome
    chroot /target apt install pardus-gnome-desktop --no-install-recommends -y
}

install_theme(){
    chroot /target apt install pardus-icon-theme pardus-gtk-theme -y

}

eval debian_"$(declare -f install_xfce)"
install_xfce(){
    debian_install_xfce
    chroot /target apt install pardus-xfce-desktop --no-install-recommends -y
}

install_libreoffice(){
    chroot /target apt install libreoffice-latests -y
}
